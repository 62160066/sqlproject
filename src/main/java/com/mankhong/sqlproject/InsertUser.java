/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.sqlproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class InsertUser {
    public static void main( String args[] ) {
      Connection connect = null;
      String dbName = "user.db";
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:"+dbName);
         connect.setAutoCommit(false);
         stmt = connect.createStatement();
         String sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (4, 'user3', 'password' );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO user (id,username,password) " +
                        "VALUES (5, 'user4', 'password' );"; 
         stmt.executeUpdate(sql);

         

         
         stmt.close();
         connect.commit();
         connect.close();
      } catch (ClassNotFoundException ex) {
          Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE,
                    null,ex );
      } catch(SQLException ex){
          Logger.getLogger(InsertUser.class.getName()).log(Level.SEVERE,
                    null,ex );
      }
      
   }
}
