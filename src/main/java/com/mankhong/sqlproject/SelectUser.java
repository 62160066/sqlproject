/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.sqlproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class SelectUser {
    public static void main( String args[] ) {
      Connection connect = null;
      String dbName = "user.db";
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:"+dbName);
         connect.setAutoCommit(false);
         stmt = connect.createStatement();
         ResultSet rs = stmt.executeQuery( "SELECT * FROM user;" ); 
         while ( rs.next() ) {
         int id = rs.getInt("id");
         String  username = rs.getString("username");
         String  password = rs.getString("password");
         
         
         
         System.out.println( "ID = " + id );
         System.out.println( "USERNAME = " + username );
         System.out.println( "PASSWORD = " + password );
         System.out.println();
         
        }
         

         stmt.close();
         rs.close();
         connect.close();
      } catch (ClassNotFoundException ex) {
          Logger.getLogger(SelectUser.class.getName()).log(Level.SEVERE,
                    null,ex );
      } catch(SQLException ex){
          Logger.getLogger(SelectUser.class.getName()).log(Level.SEVERE,
                    null,ex );
      }
      
   }
}
