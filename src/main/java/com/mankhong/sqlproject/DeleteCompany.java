/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.sqlproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class DeleteCompany {
    public static void main( String args[] ) {
      Connection connect = null;
      String dbName = "user.db";
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:"+dbName);
         connect.setAutoCommit(false);
         stmt = connect.createStatement();
         //update
         String sql = "DELETE from COMPANY where ID=2;";
         stmt.executeUpdate(sql); 
         connect.commit();
         //select         
         ResultSet rs = stmt.executeQuery( "SELECT * FROM COMPANY;" );          
         while ( rs.next() ) {
         int id = rs.getInt("id");
         String  name = rs.getString("name");
         int age  = rs.getInt("age");
         String  address = rs.getString("address");
         float salary = rs.getFloat("salary");
         
         System.out.println( "ID = " + id );
         System.out.println( "NAME = " + name );
         System.out.println( "AGE = " + age );
         System.out.println( "ADDRESS = " + address );
         System.out.println( "SALARY = " + salary );
         System.out.println();
        }
         

         stmt.close();
         rs.close();
         connect.close();
      } catch (ClassNotFoundException ex) {
          Logger.getLogger(DeleteCompany.class.getName()).log(Level.SEVERE,
                    null,ex );
      } catch(SQLException ex){
          Logger.getLogger(DeleteCompany.class.getName()).log(Level.SEVERE,
                    null,ex );
      }
      
   }
}
