/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.sqlproject;
import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author W.Home
 */
public class InsertCompany {
    public static void main( String args[] ) {
      Connection connect = null;
      String dbName = "user.db";
      Statement stmt = null;
      
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:"+dbName);
         connect.setAutoCommit(false);
         stmt = connect.createStatement();
         String sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                        "VALUES (1, 'Paul', 32, 'California', 20000.00 );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                  "VALUES (2, 'Allen', 25, 'Texas', 15000.00 );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                  "VALUES (3, 'Teddy', 23, 'Norway', 20000.00 );"; 
         stmt.executeUpdate(sql);

         sql = "INSERT INTO COMPANY (ID,NAME,AGE,ADDRESS,SALARY) " +
                  "VALUES (4, 'Mark', 25, 'Rich-Mond ', 65000.00 );"; 
         stmt.executeUpdate(sql);

         stmt.close();
         connect.commit();
         connect.close();
      } catch (ClassNotFoundException ex) {
          Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE,
                    null,ex );
      } catch(SQLException ex){
          Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE,
                    null,ex );
      }
      
   }
    
}
