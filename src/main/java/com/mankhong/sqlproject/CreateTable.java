/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mankhong.sqlproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.*;

/**
 *
 * @author W.Home
 */
public class CreateTable {
    public static void main( String args[] ) {
      Connection connect = null;
      Statement stmt = null;
      String dbName = "user.db";
      
      try {
         Class.forName("org.sqlite.JDBC");
         connect = DriverManager.getConnection("jdbc:sqlite:"+dbName);
         stmt = connect.createStatement();
         String sql = "CREATE TABLE COMPANY " +
                        "(ID INT PRIMARY KEY     NOT NULL," +
                        " NAME           TEXT    NOT NULL, " + 
                        " AGE            INT     NOT NULL, " + 
                        " ADDRESS        CHAR(50), " + 
                        " SALARY         REAL)"; 
         stmt.executeUpdate(sql);
         stmt.close();
         connect.close();
      } catch (ClassNotFoundException e ) {
         System.err.println( "Libary.org.sqlite.JDBC.bot.found!!!" );
         System.exit(0);
      } catch(SQLException ex){
         System.out.println("Unable to open Database!!!");
         System.exit(0);
      }
      System.out.println("Opened database successfully");
    }
}
